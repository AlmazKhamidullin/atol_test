/*
 * atol.h
 *
 *  Created on: 19 ���. 2019 �.
 *      Author: almaz
 */

#ifndef YANDEX_MEM_H_
#define YANDEX_MEM_H_

// no any hardware dependent headers to be used in "yandex_mem" module.
// It gives as possibility to use it in another projects as library.
#include <stdint.h>

// here we can configure static memory block and pool size
#define BLOCK_SIZE		2
#define BLOCKS_AMOUNT	4

#define POOL_SIZE	BLOCK_SIZE*BLOCKS_AMOUNT

//uint8_t* yandex_mem_stat_alloc_block(void);
uint8_t* yandex_mem_stat_alloc_block(uint32_t* allocated_cnt);
uint8_t yandex_mem_free_block(uint8_t* ptr);
uint8_t yandex_mem_free_block_debug(uint32_t block_num); // for debugging purposes. need to be commented on release.
uint8_t* yandex_unsafe_get_mem_stack(); // for debugging purposes. need to be commented on release.

#endif /* YANDEX_MEM_H_ */
