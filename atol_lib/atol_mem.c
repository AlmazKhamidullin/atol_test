/*
 * atol.c
 *
 *  Created on: 19 ���. 2019 �.
 *      Author: almaz
 */

#include <atol_mem.h>

static uint8_t mem_pool[POOL_SIZE] = {0};
static uint32_t mem_flags[BLOCKS_AMOUNT] = {0};

static uint32_t yandex_mem_find_empty_block(void) // we return ptr to memory block for use
{
	for(uint32_t i=0; i<BLOCKS_AMOUNT; i++) {
		if(mem_flags[i] == 0) {
			return i;	//return first free memory block
		}
	}
	return BLOCKS_AMOUNT; // seems that all memory used
}

uint8_t* yandex_mem_stat_alloc_block(uint32_t* allocated_cnt)
{
	uint32_t cur_block_number = yandex_mem_find_empty_block();
	if( cur_block_number == BLOCKS_AMOUNT ) {
		return 0; //we don't have free memory block
	}

	uint8_t* ptr = mem_pool + cur_block_number * BLOCK_SIZE;
	mem_flags[cur_block_number] = 1; // mark block as used
	*allocated_cnt = BLOCK_SIZE;
	return ptr;
}

uint8_t yandex_mem_free_block(uint8_t* ptr) // considering that we get back ptr we gave earlier
{
	if( ptr < mem_pool ) {
		return 0; // ptr out of bounds
	}
	if( ptr >= mem_pool + POOL_SIZE ) {
		return 0; // ptr out of bounds
	}
	if( (ptr - mem_pool)%BLOCK_SIZE ) {
		return 0; // unaligned ptr.
	}
	uint32_t cur_block_number = ( ptr - mem_pool ) / BLOCK_SIZE;
	mem_flags[cur_block_number] = 0;
	memset(ptr, 0, BLOCK_SIZE);
	return 1;
}

uint8_t yandex_mem_free_block_debug(uint32_t block_num) // for debugging purposes. need to be commented on release.
{
	if(block_num >= BLOCKS_AMOUNT) {
		return 0;
	}
	if(mem_flags[block_num] == 0) {
		return 1; // already cleaned
	}
	mem_flags[block_num] = 0;
	memset(mem_pool + block_num*BLOCK_SIZE, 0, BLOCK_SIZE);
	return 1;
}

uint8_t* yandex_unsafe_get_mem_stack() // for debugging purposes. need to be commented on release.
{
	return mem_pool;
}

