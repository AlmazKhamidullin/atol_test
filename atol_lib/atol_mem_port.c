/*
 * atol_uart.c
 *
 *  Created on: 19 ���. 2019 �.
 *      Author: almaz
 */

#include <atol_mem_port.h>	// here is our port, e.g. hardware dependent part of code. We use FreeRTOS.

#define UART_FIFO_TX_SIZE                                8
#define UART_FIFO_RX_SIZE                                256

uint8_t UartTxBuffer[UART_FIFO_TX_SIZE];
uint8_t UartRxBuffer[UART_FIFO_RX_SIZE];

xSemaphoreHandle mem_semaphore;

void yandex_mem_init(void)
{
	vSemaphoreCreateBinary( mem_semaphore );
}

uint8_t* yandex_mem_alloc(uint32_t* allocated_size)
{
	xSemaphoreTake( mem_semaphore, portMAX_DELAY );				//protection in a case of multitask operations
	uint8_t* ptr = yandex_mem_stat_alloc_block(allocated_size);
	xSemaphoreGive( mem_semaphore );
	return ptr;
}

uint8_t yandex_mem_free(uint8_t* ptr)
{
	return yandex_mem_free_block(ptr);
}

void _yandex_test()
{
	uint32_t size = 0;
	uint8_t* ptr = yandex_mem_alloc(&size);
	TEST_ASSERT_EQUAL_INT(BLOCK_SIZE, size); 	// out of bound
	TEST_ASSERT_EQUAL_INT(0, yandex_mem_free_block(ptr + POOL_SIZE + BLOCK_SIZE)); 	// out of bound
	TEST_ASSERT_EQUAL_INT(0, yandex_mem_free_block(ptr + POOL_SIZE)); 						// out of bound
	TEST_ASSERT_EQUAL_INT(1, yandex_mem_free_block(ptr + POOL_SIZE - BLOCK_SIZE)); 	// cleaned successfully. not forbidden to clean already cleaned data
	TEST_ASSERT_EQUAL_INT(1, yandex_mem_free_block(ptr)); 											// cleaned successfully.
}
void yandex_rx_buf_TEST() // used library - http://www.throwtheswitch.org/unity
{
	UnityBegin("yandex_mem_port.c");
	RUN_TEST(_yandex_test, 20);
	UnityEnd();
}

