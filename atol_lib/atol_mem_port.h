/*
 * yandex_uart.h
 *
 *  Created on: 19 ���. 2019 �.
 *      Author: almaz
 */

#ifndef YANDEX_UART_H_
#define YANDEX_UART_H_

// hardware dependent "atol_mem_port" implementation for stm32F4 series
// includes hardware independent "yandex_mem" module.

#include "cmsis_os.h"	// here is our port, e.g. hardware dependent part of code. For stm32F4, FreeRTOS driven.

#include "atol_mem.h"
#include "unity.h"

#define UART_RX_PACKET_SIZE		BLOCK_SIZE
#define UART_RX_BUF_SIZE		POOL_SIZE

void yandex_mem_init(void);
uint8_t* yandex_mem_alloc(uint32_t* allocated_size);
#endif /* YANDEX_UART_H_ */
